package com.keyi;

import com.keyi.entity.Students;
import com.keyi.service.StudentsService;
import com.keyi.util.IdGeneratorSnowflake;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author PJQ
 * @className SpringBootMutlApplicationTest
 * @Date 2022/7/21 10:24
 * @Version 1.0
 **/
@SpringBootTest
public class SpringBootMutlApplicationTest {

    @Autowired
    private StudentsService studentsService;

    @Test
    void contextLoads() {

        Students students = Students.builder().studentId(String.valueOf(new IdGeneratorSnowflake().snowflakeId())).studentName("哈喽111").studentAge(20).studentScore(88.00d).studentBirthday(new Date()).build();
        studentsService.insertOne(students);
    }

}
