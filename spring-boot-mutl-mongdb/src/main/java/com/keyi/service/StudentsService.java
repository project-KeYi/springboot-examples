package com.keyi.service;

import com.keyi.entity.Students;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author PJQ
 * @className studentsService
 * @Date 2022/7/19 22:39
 * @Version 1.0
 **/
@Service
public class StudentsService {

    @Autowired
    @Qualifier("secondaryMongoTemplate")
    private MongoTemplate mongoTemplate;



    /**
     * 插入数据
     */
    public void insertStudents(){
        for (Integer count = 0; count < 10; count++) {
            Students student = new Students()
                    .setStudentId("study_"+count) //如果自己不去设置id则系统会分配给一个id
                    .setStudentName("Echo"+count)
                    .setStudentAge(count)
                    .setStudentScore(98.5-count)
                    .setStudentBirthday(new Date());
            mongoTemplate.save(student);
        }

    }

    public void insertOne(Students students){
        mongoTemplate.save(students);
    }

    public Long updateStudent(String id){
        Query query = new Query();
        query.addCriteria(Criteria.where("studentId").is(id));
        Update update= new Update().set("studentName", "中国加油");
        UpdateResult upsert = mongoTemplate.upsert(query, update, Students.class);

        return upsert.getModifiedCount();
    }
}
