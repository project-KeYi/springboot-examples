package com.keyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author PJQ
 * @className SpringBootMutlMongdbApplication
 * @Date 2022/7/21 10:05
 * @Version 1.0
 **/
@SpringBootApplication
public class SpringBootMutlMongdbApplication {


    public static void main(String[] args) {
        SpringApplication.run(SpringBootMutlMongdbApplication.class, args);
    }
}
