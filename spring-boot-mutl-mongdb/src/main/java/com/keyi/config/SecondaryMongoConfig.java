package com.keyi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author PJQ
 * @className SecondaryMongoConfig
 * @Date 2022/7/21 14:08
 * @Version 1.0
 **/
@Configuration
@EnableMongoRepositories(basePackages = "com.keyi",
        mongoTemplateRef = SecondaryMongoConfig.MONGO_TEMPLATE)
public class SecondaryMongoConfig {

    protected static final String MONGO_TEMPLATE = "secondaryMongoTemplate";
}
