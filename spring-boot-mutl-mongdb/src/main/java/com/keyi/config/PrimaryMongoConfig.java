package com.keyi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author PJQ
 * @className PrimaryMongoConfig
 * @Date 2022/7/21 14:08
 * @Version 1.0
 **/
@Configuration
@EnableMongoRepositories(basePackages = "com.keyi",
        mongoTemplateRef = PrimaryMongoConfig.MONGO_TEMPLATE)
public class PrimaryMongoConfig {
    protected static final String MONGO_TEMPLATE = "primaryMongoTemplate";
}
