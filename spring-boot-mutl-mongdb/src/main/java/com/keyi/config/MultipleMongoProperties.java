package com.keyi.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author PJQ
 * @className MultipleMongoProperties
 * @Date 2022/7/21 14:11
 * @Version 1.0
 **/

@ConfigurationProperties(prefix = "mongodb")
@Configuration
@Getter
@Setter
@ToString
public class MultipleMongoProperties {
    private MongoProperties primary = new MongoProperties();
    private MongoProperties secondary = new MongoProperties();

}
