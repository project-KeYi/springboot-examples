package com.keyi;

import cn.hutool.core.lang.generator.SnowflakeGenerator;
import com.keyi.entity.Students;
import com.keyi.service.StudentsService;
import com.keyi.util.IdGeneratorSnowflake;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class SpringBootMongdbApplicationTests {

    @Autowired
    private StudentsService studentsService;

    @Test
    void contextLoads() {

        studentsService.insertStudents();
    }
    @Test
    void insertOne() {

//        Students students = new Students();
        Students students = Students.builder().studentId(String.valueOf(new IdGeneratorSnowflake().snowflakeId())).studentName("哈喽").studentAge(20).studentScore(88.00d).studentBirthday(new Date()).build();
        studentsService.insertOne(students);
    }
    @Test
    void updateOne(){
        studentsService.updateStudent("1549595887687303168");
    }


}
