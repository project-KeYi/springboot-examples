package com.keyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMongdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMongdbApplication.class, args);
    }

}
