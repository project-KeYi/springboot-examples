package com.keyi.dao;

import com.keyi.entity.Students;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author PJQ
 * @interfaceName StudentsDao
 * @Date 2022/7/19 22:37
 * @Version 1.0
 **/

@Repository
public interface StudentsDao extends MongoRepository<Students, String> {

    //    可根据需求自己定义方法, 无需对方法进行实现
    Students getAllByStudentName(String studentName);
}
