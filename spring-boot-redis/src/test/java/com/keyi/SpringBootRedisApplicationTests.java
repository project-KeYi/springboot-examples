package com.keyi;

import com.keyi.entity.User;
import com.keyi.service.UserService;
import com.keyi.util.IdGeneratorSnowflake;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootRedisApplicationTests {

    @Autowired
    private IdGeneratorSnowflake idGeneratorSnowflake;

    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
        User user = new User();
        user.setId(idGeneratorSnowflake.snowflakeId());
        user.setName("大聪明");
        user.setAge(20);
        userService.setUserInfo(user);

        User userInfo = userService.getUserInfo(user.getId());
        System.out.println(userInfo);

    }

}
