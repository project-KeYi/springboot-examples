package com.keyi.demo;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ZhangWuJi extends AbstractSwordsman {

    private String name;

    private String age;
    @Override
    protected void neiGong() {
        System.out.println("运行九阳神功");
    }

    public ZhangWuJi(String name, String age) {
        super();
        this.name = name;
        this.age = age;
    }

    public ZhangWuJi() {
    }

    @Override
    protected void weapons() {
        System.out.println("没有武器");
    }

    @Override
    protected void moves() {
        System.out.println("使用招式乾坤大挪移");
    }

    @Override
    protected boolean hasWeapons() {
        return false;
    }

    public static void main(String[] args) {
        //ZhangWuJi zhangWuJi = new ZhangWuJi("nihao","111");
        //zhangWuJi.fighting();

       AbstractSwordsman zhangWuJi1 = new ZhangWuJi();
       zhangWuJi1.fighting();

        String pattern = "^[A-Za-z0-9]{1,50}?$";
        System.err.println(Pattern.compile(pattern).matcher("@#").matches());

        System.err.println(Pattern.compile(pattern).matcher("1231qerqerqr").matches());

        List<String> objects = new ArrayList<>();
        System.err.println(JSON.toJSONString(objects));


        //123456

        //565685496859
    }
}
