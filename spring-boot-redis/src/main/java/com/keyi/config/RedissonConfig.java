package com.keyi.config;

import io.micrometer.core.instrument.util.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private String redisPort;

    @Value("${spring.redis.password}")
    String redisPassword;

    @Value("${spring.redis.timeout}")
    private Integer redisTimeout;

    @Value("${spring.redis.pool.max-active}")
    Integer redisPoolSize;

    @Value("${spring.redis.pool.min-idle}")
    Integer redisMinIdle;


    /**
     * Redisson配置
     * @return
     */
    @Bean
    public RedissonClient redissonClient(){
        //1、创建配置
        Config config = new Config();
        redisHost = redisHost.startsWith("redis://") ? redisHost : "redis://" + redisHost;

        SingleServerConfig serverConfig = config.useSingleServer()
                .setAddress(redisHost + ":" + redisPort)
                .setTimeout(redisTimeout)
                .setConnectionPoolSize(redisPoolSize)
                .setConnectionMinimumIdleSize(redisMinIdle);
        if (StringUtils.isNotBlank(redisPassword)) {
            serverConfig.setPassword(redisPassword);
        }

        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;
    }

    @Deprecated
    public RedissonClient clusterRedissonClient(){
        Config config = new Config();
        config.useClusterServers()
                .setScanInterval(2000) // 集群状态扫描间隔时间，单位是毫秒
                //可以用"rediss://"来启用SSL连接
                .addNodeAddress("redis://127.0.0.1:7000", "redis://127.0.0.1:7001")
                .addNodeAddress("redis://127.0.0.1:7002");

        RedissonClient redisson = Redisson.create(config);

        return redisson;
    }

}
