package com.keyi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author PJQ
 * @className User
 * @Date 2022/7/17 13:40
 * @Version 1.0
 **/
@Getter
@Setter
@ToString
public class User {

    private Long id;

    private String name;

    private Integer age;

}
