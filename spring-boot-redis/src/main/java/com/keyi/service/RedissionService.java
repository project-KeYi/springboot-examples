package com.keyi.service;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedissionService {

    @Autowired
    private RedissonClient redissonClient;

    public void TestValid(Long id){
        // 1.制定锁key
        String lockKey = "buy:product:" + id;

        RLock lock = redissonClient.getLock(lockKey);

        try {
            //3.获取锁，如果被占用，则等待
            boolean res = lock.tryLock(10, TimeUnit.SECONDS);


            if (res){
                //处理对应的业务。
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }


    }
}
