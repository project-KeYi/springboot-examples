package com.keyi.service;

import com.keyi.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author PJQ
 * @className UserService
 * @Date 2022/7/17 13:57
 * @Version 1.0
 **/
@Service
public class UserService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public User getUserInfo(Long id){
        String key  = "user:id:" + id;
        User o = (User) redisTemplate.opsForValue().get(key);
        return o;
    }

    public void setUserInfo(User user){
        Long id = user.getId();
        String key  = "user:id:" + id;
        redisTemplate.opsForValue().set(key, user);

    }
}
